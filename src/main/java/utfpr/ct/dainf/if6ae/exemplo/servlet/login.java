package utfpr.ct.dainf.if6ae.exemplo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Universidade Tecnológica Federal do Paraná
 * IF6AE Desenvolvimento de Aplicaçoões Web
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet { 

    /**
     * Processa requisições HTTP para os métodos
     * <code>GET</code> e
     * <code>POST</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
                    + "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            out.println("<html>");
            out.println("<head>"); 
            out.println("<title>Formulário de Login</title>");            
            out.println("</head>");
            out.println("<body>");
            
            String login = request.getParameter("login");
            String senha = request.getParameter("senha");
            String perfil = request.getParameter("perfil");
           
            if  (request.getMethod().equalsIgnoreCase("post")) {
                if (login == null || login.trim().isEmpty()) {
                    out.println("<h2 style='color: red'>Informe o login!</h2>");
                } else if (senha == null || senha.trim().isEmpty()){
                    out.println("<h2 style='color: red'>Informe o senha!</h2>");
                } else if (perfil == null || perfil.trim().isEmpty()) {
                    out.println("<h2 style='color: red'>Informe o perfil!</h2>");
                } else if(login.equals(senha)){
                        response.sendRedirect("./sucesso?login="+login+"&perfil="+perfil+"");
                        //request.getRequestDispatcher("./sucesso").forward(request, response);
                        //out.println("<h1>Acesso a perfil: " + perfil + " e login: " + login + " permitido.</h1>");
                        //out.println("<h2>Olá, seja bem vindo, " + login + "</h2>");                    
                }else{
                    response.sendRedirect("erro.xhtml");
                    //request.getRequestDispatcher("erro.xhtml").forward(request, response);
                    //out.println("<h1>Login Inválido.</h1>");
                }
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Processa a requisição HTTP para o método
     * <code>GET</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Processa a requisição HTTP para o método
     * <code>POST</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Retorna uma descrição resumida do servlet.
     * @return Uma descrição resumida.
     */
    @Override
    public String getServletInfo() {
        return "Exemplo de servlet simples";
    }// </editor-fold>
}
